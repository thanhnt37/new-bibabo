$('#event_start_at').datetimepicker({'format': 'YYYY-MM-DD HH:mm:ss', 'defaultDate': new Date()});
$('#event_end_at').datetimepicker({'format': 'YYYY-MM-DD HH:mm:ss'});

$(document).ready(function () {
    $('#event_name').froalaEditor({
        toolbarInline: false,
        pastePlain: true,
        heightMin: 100,
        heightMax: 200
    });

    $('#event_description').froalaEditor({
        toolbarInline: false,
        pastePlain: true,
        heightMin: 300,
        heightMax: 650
    });

    $(".event_club_ids").select2({
        placeholder: "Chọn hội được áp dụng sự kiện",
        allowClear: true
    }).val(Boilerplate.clubs).trigger("change");

    $('#cover-image').change(function (event) {
        $('#cover-image-preview').attr('src', URL.createObjectURL(event.target.files[0]));
    });
});