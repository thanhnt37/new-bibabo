@extends('pages.admin.' . config('view.admin') . '.layout.application', ['menu' => 'events'] )

@section('metadata')
@stop

@section('styles')
    <style>
        .event-general-info {
            list-style: none;
            padding-left: 10px;
        }
        .event-general-info li {
            margin-bottom: 10px;
        }
    </style>
@stop

@section('scripts')
<script src="{!! \URLHelper::asset('js/delete_item.js', 'admin/adminlte') !!}"></script>
@stop

@section('title')
@stop

@section('header')
Events
@stop

@section('breadcrumb')
<li class="active">Events</li>
@stop

@section('content')
<div class="box box-primary">
    <div class="box-header with-border">

        <div class="row">
            <div class="col-sm-6">
                <h3 class="box-title">
                    <p class="text-right">
                        <a href="{!! action('Admin\EventController@create') !!}" class="btn btn-block btn-primary btn-sm" style="width: 125px;">@lang('admin.pages.common.buttons.create')</a>
                    </p>
                </h3>
                <br>
                <p style="display: inline-block;">@lang('admin.pages.common.label.search_results', ['count' => $count])</p>
            </div>
            <div class="col-sm-6 wrap-top-pagination">
                <div class="heading-page-pagination">
                    {!! \PaginationHelper::render($paginate['order'], $paginate['direction'], $paginate['offset'], $paginate['limit'], $count, $paginate['baseUrl'], [], $count, 'shared.topPagination') !!}
                </div>
            </div>
        </div>
    </div>
    <div class="box-body" style=" overflow-x: scroll; ">
        <table class="table table-bordered">
            <tr>
                <th style="width: 10px">{!! \PaginationHelper::sort('topic_event_id', 'ID') !!}</th>
                <th>{!! \PaginationHelper::sort('event_name', trans('admin.pages.events.columns.event_name')) !!}</th>
                <th>@lang('admin.pages.events.columns.general_info')</th>
                <th>@lang('admin.pages.events.columns.activities')</th>
                <th>@lang('admin.pages.events.columns.author')</th>

                <th>{!! \PaginationHelper::sort('event_status', trans('admin.pages.events.columns.event_status')) !!}</th>
                <th style="width: 40px">@lang('admin.pages.common.label.actions')</th>
            </tr>
            @foreach( $events as $event )
                <tr>
                    <td>{{ $event->topic_event_id }}</td>
                    <td><a href="#">{!! $event->event_name !!}</a></td>

                    <td>
                        <ul class="event-general-info">
                            <li>Start: <i>{{ $event->event_start_at }}</i></li>
                            <li>End: <i>{{ $event->event_end_at }}</i></li>
                            <li>Type: <i>{{ $event->present()->getEventType() }}</i></li>
                        </ul>
                    </td>

                    <td>
                        <ul class="event-general-info">
                            <li>
                                Subscribe:
                                <i>
                                    @php  $subscribes = $event->present()->getTopic()->present()->getNumberSubscribed(); @endphp
                                    {{$subscribes}}
                                </i>
                            </li>
                            <li>
                                Participants:
                                <i>
                                    @php  $participants = $event->present()->getTopic()->present()->getNumberParticipants(); @endphp
                                    {{$participants}}
                                </i>
                            </li>
                        </ul>
                    </td>

                    <td>
                        <ul class="event-general-info">
                            <li>
                                Create by:
                                <i>
                                    @php  $creator = $event->present()->getTopic()->present()->getAuthor(); @endphp
                                    @if(isset($creator->user_alias))
                                        <a href="#">{{$creator->user_alias}}</a>
                                    @else
                                        Unknown
                                    @endif
                                </i>
                            </li>
                            <li>Created at: <i>{{ $event->event_created_at }}</i></li>
                        </ul>
                    </td>

                    <td>
                        @if( $event->event_status )
                            <span class="badge bg-green">Publish</span>
                        @else
                            <span class="badge bg-red">UnPublish</span>
                        @endif
                    </td>

                    <td>
                        <a href="{!! action('Admin\EventController@show', $event->topic_event_id) !!}"
                           class="btn btn-block btn-primary btn-xs">@lang('admin.pages.common.buttons.edit')</a>
                        <a href="#" class="btn btn-block btn-danger btn-xs delete-button"
                           data-delete-url="{!! action('Admin\EventController@destroy', $event->topic_event_id) !!}">@lang('admin.pages.common.buttons.delete')</a>
                    </td>
                </tr>
            @endforeach
        </table>
    </div>
    <div class="box-footer">
        {!! \PaginationHelper::render($paginate['order'], $paginate['direction'], $paginate['offset'], $paginate['limit'], $count, $paginate['baseUrl'], []) !!}
    </div>
</div>
@stop