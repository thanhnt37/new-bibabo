@extends('pages.admin.' . config('view.admin') . '.layout.application', ['menu' => 'events'] )

@section('metadata')
@stop

@section('styles')
    <link rel="stylesheet" href="{!! \URLHelper::asset('libs/datetimepicker/css/bootstrap-datetimepicker.min.css', 'admin') !!}">

    <link rel="stylesheet" href="{!! \URLHelper::asset('libs/plugins/select2/select2.min.css', 'admin') !!}">

    <!-- Include Froala Editor style. -->
    <link href='https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.3.5/css/froala_editor.pkgd.min.css' rel='stylesheet' type='text/css' />
    <link href='https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.3.5/css/froala_style.min.css' rel='stylesheet' type='text/css' />
@stop

@section('scripts')
    <script>
        Boilerplate.clubs = [{{$event->event_club_ids}}];
    </script>

    <script src="{{ \URLHelper::asset('libs/moment/moment.min.js', 'admin') }}"></script>
    <script src="{{ \URLHelper::asset('libs/datetimepicker/js/bootstrap-datetimepicker.min.js', 'admin') }}"></script>
    <script src="{{ \URLHelper::asset('libs/plugins/select2/select2.full.min.js', 'admin') }}"></script>

    <!-- Include Froala JS file. -->
    <script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.3.5/js/froala_editor.pkgd.min.js'></script>

    <script src="{{ \URLHelper::asset('js/pages/events/edit.js', 'admin/adminlte') }}"></script>
@stop

@section('title')
@stop

@section('header')
    Events
@stop

@section('breadcrumb')
    <li><a href="{!! action('Admin\EventController@index') !!}"><i class="fa fa-files-o"></i> Events</a></li>
    @if( $isNew )
        <li class="active">New</li>
    @else
        <li class="active">{{ $event->id }}</li>
    @endif
@stop

@section('content')
    @if (count($errors) > 0)
        <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form action="@if($isNew) {!! action('Admin\EventController@store') !!} @else {!! action('Admin\EventController@update', [$event->topic_event_id]) !!} @endif" method="POST" enctype="multipart/form-data">
        @if( !$isNew ) <input type="hidden" name="_method" value="PUT"> @endif
        {!! csrf_field() !!}

            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">
                        <a href="{!! URL::action('Admin\EventController@index') !!}"
                           class="btn btn-block btn-default btn-sm"
                           style="width: 125px;">@lang('admin.pages.common.buttons.back')</a>
                    </h3>
                </div>

                <div class="box-body">
                    <div class="bs-example" style="padding: 10px;" data-example-id="simple-nav-tabs">
                        <ul class="nav nav-tabs event-tabs">
                            <li role="presentation" class="active"><a href="#">General</a></li>
                            <li role="presentation"><a href="{{action('Admin\EventController@participants', $event->topic_event_id)}}">Participants</a></li>
                        </ul>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group @if ($errors->has('event_name')) has-error @endif">
                                <label for="event_name">@lang('admin.pages.events.columns.event_name')</label>
                                <textarea id="event_name" name="event_name" class="form-control" required placeholder="Tiêu đề cuộc thi">{{ old('event_name') ? old('event_name') : $event->event_name }}</textarea>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group text-center">
                                @if( !empty($event->event_banner_url))
                                    <img id="cover-image-preview" style="max-width: 500px; width: 100%;" src="{!! $event->event_banner_url !!}" alt="" class="margin"/>
                                @else
                                    <img id="cover-image-preview" style="max-width: 500px; width: 100%;" src="http://placehold.it/800x450" alt="" class="margin"/>
                                @endif
                                <input type="file" style="display: none;" id="cover-image" name="cover_image">
                                <p class="help-block" style="font-weight: bolder;">
                                    Cover Image
                                    <label for="cover-image" style="font-weight: 100; color: #549cca; margin-left: 10px; cursor: pointer;">@lang('admin.pages.common.buttons.edit')</label>
                                </p>
                            </div>
                        </div>

                        <div class="col-lg-6">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group @if ($errors->has('event_start_at')) has-error @endif">
                                        <label for="event_start_at">@lang('admin.pages.events.columns.event_start_at')</label>
                                        <div class="input-group date datetime-field">
                                            <input type="text" class="form-control" id="event_start_at" name="event_start_at" required value="{{ old('event_start_at') ? old('event_start_at') : $event->event_start_at }}">
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group @if ($errors->has('event_end_at')) has-error @endif">
                                        <label for="event_end_at">@lang('admin.pages.events.columns.event_end_at')</label>
                                        <div class="input-group date datetime-field">
                                            <input type="text" class="form-control" id="event_end_at" name="event_end_at" value="{{ old('event_end_at') ? old('event_end_at') : $event->event_end_at }}">
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="event_created_at">@lang('admin.pages.events.columns.event_created_at')</label>
                                        <input type="text" class="form-control" name="event_created_at" disabled value="{{$event->event_created_at }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group @if ($errors->has('event_like_point')) has-error @endif">
                                <label for="event_like_point">@lang('admin.pages.events.columns.event_like_point')</label>
                                <input type="number" min="0" class="form-control" id="event_like_point" name="event_like_point"
                                       required
                                       value="{{ old('event_like_point') ? old('event_like_point') : $event->event_like_point }}">
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group @if ($errors->has('event_share_point')) has-error @endif">
                                <label for="event_share_point">@lang('admin.pages.events.columns.event_share_point')</label>
                                <input type="number" min="0" class="form-control" id="event_share_point" name="event_share_point"
                                       required
                                       value="{{ old('event_share_point') ? old('event_share_point') : $event->event_share_point }}">
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group @if ($errors->has('event_comment_point')) has-error @endif">
                                <label for="event_comment_point">@lang('admin.pages.events.columns.event_comment_point')</label>
                                <input type="number" min="0" class="form-control" id="event_comment_point"
                                       name="event_comment_point" required
                                       value="{{ old('event_comment_point') ? old('event_comment_point') : $event->event_comment_point }}">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="event_status">is Published</label>
                                <div class="switch">
                                    <input type="hidden" name="event_status" value="0">
                                    <input id="event_status" name="event_status" value="1" @if( $event->event_status) checked @endif class="cmn-toggle cmn-toggle-round-flat" type="checkbox">
                                    <label for="event_status"></label>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="event_rank">Show Ranks</label>
                                <div class="switch">
                                    <input type="hidden" name="event_rank" value="0">
                                    <input id="event_rank" name="event_rank" value="1" @if( $event->event_rank) checked @endif class="cmn-toggle cmn-toggle-round-flat" type="checkbox">
                                    <label for="event_rank"></label>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="onoffswitch">
                                <label for="event_type">Contest</label>
                                <input type="hidden" name="event_type" value="0">
                                <input type="checkbox" name="event_type" class="onoffswitch-checkbox" id="event_type" @if( $event->event_type) checked @endif value="1">
                                <label class="onoffswitch-label" for="event_type">
                                    <span class="onoffswitch-inner"></span>
                                    <span class="onoffswitch-switch"></span>
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group @if ($errors->has('event_club_ids')) has-error @endif">
                                <label for="event_club_ids">@lang('admin.pages.events.columns.event_club_ids')</label>
                                <select class="form-control event_club_ids" name="event_club_ids[]" required id="event_club_ids" style="margin-bottom: 15px;" multiple="multiple">
                                    @foreach( $groups as $key => $group )
                                        <option value="{!! $group->qa_topic_id !!}">
                                            {{ $group->qa_topic_name }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group @if ($errors->has('event_description')) has-error @endif">
                                <label for="event_description">Content</label>
                                <textarea id="event_description" name="event_description" class="form-control" placeholder="Nội dung thể lệ cuộc thi">{{ old('event_description') ? old('event_description') : (isset($event->topic->qa_topic_desc) ? $event->topic->qa_topic_desc : '') }}</textarea>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="box-footer">
                    <button type="submit" class="btn btn-primary btn-sm" style="width: 125px;">@lang('admin.pages.common.buttons.save')</button>
                </div>
            </div>
    </form>
@stop
