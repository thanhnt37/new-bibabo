@extends('pages.admin.' . config('view.admin') . '.layout.application', ['menu' => 'events'] )

@section('metadata')
@stop

@section('styles')
    <style>
        .example-info {
            list-style: none;
            padding-left: 10px;
        }
        .example-info li {
            margin-bottom: 10px;
        }
    </style>
@stop

@section('scripts')
@stop

@section('title')
@stop

@section('header')
    Events
@stop

@section('breadcrumb')
    <li><a href="{!! action('Admin\EventController@index') !!}"><i class="fa fa-files-o"></i> Events</a></li>
    <li class="active">{{ $event->id }}</li>
@stop

@section('content')
    @if (count($errors) > 0)
        <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">
                <a href="{!! URL::action('Admin\EventController@index') !!}"
                   class="btn btn-block btn-default btn-sm"
                   style="width: 125px;">@lang('admin.pages.common.buttons.back')</a>
            </h3>
        </div>

        <div class="box-body">
            <div class="bs-example" style="padding: 10px;" data-example-id="simple-nav-tabs">
                <ul class="nav nav-tabs event-tabs">
                    <li role="presentation">
                        <a href="{{action('Admin\EventController@show', $event->topic_event_id)}}">General</a>
                    </li>
                    <li role="presentation" class="active">
                        <a href="#">Participants</a>
                    </li>
                </ul>
            </div>

            <table class="table table-bordered">
                <tr>
                    <th>Images</th>
                    <th width="350px">Content</th>
                    <th>Information</th>
                    <th>Statistics</th>

                    <th style="width: 40px">@lang('admin.pages.common.label.actions')</th>
                </tr>
                @foreach( $examples as $example )
                    <tr>
                        <td>{{$example->question_image}}</td>

                        <td>
                            <p><strong>{{$example->question_question}}</strong></p>

                            <p>{{substr($example->question_desc, 0, 500)}} ...</p>
                        </td>

                        <td>
                            <ul class="example-info">
                                <li>
                                    ID:
                                    <i>{{$example->question_id}}</i>
                                </li>
                                <li>
                                    Author:
                                    <i>
                                        @php  $author = $example->present()->getAuthor(); @endphp
                                        {{$author->user_alias}}
                                    </i>
                                </li>
                                <li>
                                    Created:
                                    <i>{{$example->question_created_at}}</i>
                                </li>
                            </ul>
                        </td>

                        <td>
                            <ul class="example-info">
                                @php
                                    $numberLike    = (isset($example->statistics->question_statistic_number_like)) ? intval($example->statistics->question_statistic_number_like) : 0;
                                    $numberShare   = (isset($example->statistics->question_statistic_number_share_fb)) ? intval($example->statistics->question_statistic_number_share_fb) : 0;
                                    $numberComment = (isset($example->statistics->question_statistic_number_comment)) ? intval($example->statistics->question_statistic_number_comment) : 0;

                                    $like    = $event->event_like_point * $numberLike;
                                    $share   = $event->event_like_point * $numberShare;
                                    $comment = $event->event_like_point * $numberComment;
                                    $total   = $like + $share + $comment;
                                @endphp
                                <li>
                                    Total:
                                    <i>{{$total}}</i>
                                </li>
                                <li>
                                    Like:
                                    <i>{{$like}}</i>
                                </li>
                                <li>
                                    Share:
                                    <i>{{$share}}</i>
                                </li>
                                <li>
                                    Comment:
                                    <i>{{$comment}}</i>
                                </li>
                            </ul>
                        </td>

                        <td>
                            <a href="#" class="btn btn-block btn-danger btn-xs" data-delete-url="#">@lang('admin.pages.common.buttons.delete')</a>
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>

        <div class="box-footer">
        </div>
    </div>
@stop
