<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(
    App\Models\User::class,
    function (Faker\Generator $faker)
    {
        return [
            'name'                 => $faker->name,
            'email'                => $faker->email,
            'password'             => bcrypt(str_random(10)),
            'remember_token'       => str_random(10),
            'gender'               => 1,
            'telephone'            => $faker->phoneNumber,
            'birthday'             => $faker->date('Y-m-d'),
            'locale'               => $faker->languageCode,
            'address'              => $faker->address,
            'last_notification_id' => 0,
            'api_access_token'     => '',
            'profile_image_id'     => 0,
            'is_activated'         => 0,
        ];
    }
);

$factory->define(
    App\Models\AdminUser::class,
    function (Faker\Generator $faker)
    {
        return [
            'name'                 => $faker->name,
            'email'                => $faker->email,
            'password'             => bcrypt(str_random(10)),
            'remember_token'       => str_random(10),
            'locale'               => $faker->languageCode,
            'last_notification_id' => 0,
            'api_access_token'     => '',
            'profile_image_id'     => 0,
        ];
    }
);

$factory->define(
    App\Models\AdminUserRole::class,
    function (Faker\Generator $faker)
    {
        return [
            'admin_user_id' => $faker->randomNumber(),
            'role'          => 'supper_user'
        ];
    }
);

$factory->define(
    App\Models\SiteConfiguration::class,
    function (Faker\Generator $faker)
    {
        return [
            'locale'                => 'ja',
            'name'                  => $faker->name,
            'title'                 => $faker->sentence,
            'keywords'              => implode(',', $faker->words(5)),
            'description'           => $faker->sentences(3, true),
            'ogp_image_id'          => 0,
            'twitter_card_image_id' => 0,
        ];
    }
);

$factory->define(
    App\Models\Image::class,
    function (Faker\Generator $faker)
    {
        return [
            'url'                => $faker->imageUrl(),
            'title'              => $faker->sentence,
            'is_local'           => false,
            'entity_type'        => $faker->word,
            'entity_id'          => 0,
            'file_category_type' => $faker->word,
            's3_key'             => $faker->word,
            's3_bucket'          => $faker->word,
            's3_region'          => $faker->word,
            's3_extension'       => 'png',
            'media_type'         => 'image/png',
            'format'             => 'png',
            'file_size'          => 0,
            'width'              => 100,
            'height'             => 100,
            'is_enabled'         => true,
        ];
    }
);

$factory->define(
    App\Models\Article::class,
    function (Faker\Generator $faker)
    {
        return [
            'slug'               => $faker->word,
            'title'              => $faker->sentence,
            'keywords'           => implode(',', $faker->words(5)),
            'description'        => $faker->sentences(3, true),
            'content'            => $faker->sentences(3, true),
            'cover_image_id'     => 0,
            'locale'             => 'ja',
            'is_enabled'         => true,
            'publish_started_at' => $faker->dateTime,
            'publish_ended_at'   => null,
        ];
    }
);

$factory->define(
    App\Models\UserNotification::class,
    function (Faker\Generator $faker)
    {
        return [
            'user_id'       => \App\Models\UserNotification::BROADCAST_USER_ID,
            'category_type' => \App\Models\UserNotification::CATEGORY_TYPE_SYSTEM_MESSAGE,
            'type'          => \App\Models\UserNotification::TYPE_GENERAL_MESSAGE,
            'data'          => '',
            'locale'        => 'en',
            'content'       => 'TEST',
            'read'          => false,
            'sent_at'       => $faker->dateTime,
        ];
    }
);

$factory->define(
    App\Models\AdminUserNotification::class,
    function (Faker\Generator $faker)
    {
        return [
            'user_id'       => \App\Models\AdminUserNotification::BROADCAST_USER_ID,
            'category_type' => \App\Models\AdminUserNotification::CATEGORY_TYPE_SYSTEM_MESSAGE,
            'type'          => \App\Models\AdminUserNotification::TYPE_GENERAL_MESSAGE,
            'data'          => '',
            'locale'        => 'en',
            'content'       => 'TEST',
            'read'          => false,
            'sent_at'       => $faker->dateTime,
        ];
    }
);

$factory->define(App\Models\Event::class, function (Faker\Generator $faker)
{
    return [
        'topic_event_id'      => 0,
        'qa_topic_id'         => 0,
        'event_name'          => $faker->sentence,
        'event_template_name' => $faker->sentence,
        'event_master_id'     => 0,
        'event_status'        => 0,
        'event_type'          => 0,
        'event_created_at'    => $faker->dateTime,
        'event_start_at'      => $faker->dateTime,
        'event_end_at'        => $faker->dateTime,
        'event_priority_qids' => '',
        'event_product_ids'   => '',
        'event_banner_url'    => $faker->image(),
        'event_rank'          => 1,
        'event_like_point'    => $faker->randomDigit,
        'event_share_point'   => $faker->randomDigit,
        'event_comment_point' => $faker->randomDigit,
        'event_club_ids'      => '',
    ];
});

$factory->define(App\Models\QaTopic::class, function (Faker\Generator $faker)
{
    return [
        'qa_topic_id'                => '',
        'qa_topic_is_parent'         => '',
        'qa_topic_parent_id'         => '',
        'qa_topic_deep_level'        => '',
        'qa_topic_list_all_child_id' => '',
        'qa_topic_sort'              => '',
        'qa_topic_name'              => '',
        'qa_topic_shortname'         => '',
        'qa_topic_fullname'          => '',
        'qa_topic_desc'              => '',
        'qa_topic_parent'            => '',
        'qa_topic_fixed'             => '',
        'qa_topic_month'             => '',
        'qa_topic_number_question'   => '',
        'qa_topic_number_answer'     => '',
        'qa_topic_number_like'       => '',
        'qa_topic_number_assistant'  => '',
        'language_id'                => '',
        'qa_topic_status'            => '',
        'qa_topic_created_at'        => '',
        'admin_id'                   => '',
        'user_id'                    => '',
        'qa_topic_image'             => '',
        'qa_topic_del_flag'          => '',
        'qa_topic_type'              => '',
        'qa_topic_category'          => '',
    ];
});

$factory->define(App\Models\Question::class, function (Faker\Generator $faker)
{
    return [
        'question_id'                  => '',
        'question_question'            => '',
        'question_desc'                => '',
        'question_image'               => '',
        'content_type'                 => '',
        'question_approved_user_level' => '',
        'user_id'                      => '',
        'month_no'                     => '',
        'week_no'                      => '',
        'question_created_at'          => '',
        'question_del_flag'            => '',
        'question_type'                => '',
        'question_status'              => '',
        'language_id'                  => '',
        'question_last_action'         => '',
        'qa_club_id'                   => '',
        'assigned_mod_id'              => '',
    ];
});

$factory->define(App\Models\QaTopicLike::class, function (Faker\Generator $faker)
{
    return [
        'qa_topic_like_id'         => '',
        'user_id'                  => '',
        'qa_topic_id'              => '',
        'qa_topic_like_is_unlike'  => '',
        'qa_topic_like_created_at' => '',
    ];
});

$factory->define(App\Models\QuestionStatistic::class, function (Faker\Generator $faker)
{
    return [
        'question_id'                            => '',
        'user_id'                                => '',
        'question_created_at'                    => '',
        'question_statistic_number_viewed'       => '',
        'question_statistic_number_viewed_ano'   => '',
        'question_statistic_number_like'         => '',
        'question_statistic_number_answer'       => '',
        'question_statistic_number_like_answer'  => '',
        'question_statistic_number_comment'      => '',
        'question_statistic_number_like_comment' => '',
        'question_statistic_number_share_fb'     => '',
        'question_statistic_latitude'            => '',
        'question_statistic_longitude'           => '',
        'question_statistic_answer_hot'          => '',
        'first_answer_at'                        => '',
        'question_statistic_created_platform'    => '',
        'language_id'                            => '',
        'question_statistic_del_flag'            => '',
        'feed_extra_answer_id'                   => '',
        'on_create_send_notify_count'            => '',
        'question_statistic_saved_count'         => '',
    ];
});

/* NEW MODEL FACTORY */
