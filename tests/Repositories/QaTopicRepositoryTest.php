<?php namespace Tests\Repositories;

use App\Models\QaTopic;
use Tests\TestCase;

class QaTopicRepositoryTest extends TestCase
{
    protected $useDatabase = true;

    public function testGetInstance()
    {
        /** @var  \App\Repositories\QaTopicRepositoryInterface $repository */
        $repository = \App::make(\App\Repositories\QaTopicRepositoryInterface::class);
        $this->assertNotNull($repository);
    }

    public function testGetList()
    {
        $qaTopics = factory(QaTopic::class, 3)->create();
        $qaTopicIds = $qaTopics->pluck('id')->toArray();

        /** @var  \App\Repositories\QaTopicRepositoryInterface $repository */
        $repository = \App::make(\App\Repositories\QaTopicRepositoryInterface::class);
        $this->assertNotNull($repository);

        $qaTopicsCheck = $repository->get('id', 'asc', 0, 1);
        $this->assertInstanceOf(QaTopic::class, $qaTopicsCheck[0]);

        $qaTopicsCheck = $repository->getByIds($qaTopicIds);
        $this->assertEquals(3, count($qaTopicsCheck));
    }

    public function testFind()
    {
        $qaTopics = factory(QaTopic::class, 3)->create();
        $qaTopicIds = $qaTopics->pluck('id')->toArray();

        /** @var  \App\Repositories\QaTopicRepositoryInterface $repository */
        $repository = \App::make(\App\Repositories\QaTopicRepositoryInterface::class);
        $this->assertNotNull($repository);

        $qaTopicCheck = $repository->find($qaTopicIds[0]);
        $this->assertEquals($qaTopicIds[0], $qaTopicCheck->id);
    }

    public function testCreate()
    {
        $qaTopicData = factory(QaTopic::class)->make();

        /** @var  \App\Repositories\QaTopicRepositoryInterface $repository */
        $repository = \App::make(\App\Repositories\QaTopicRepositoryInterface::class);
        $this->assertNotNull($repository);

        $qaTopicCheck = $repository->create($qaTopicData->toFillableArray());
        $this->assertNotNull($qaTopicCheck);
    }

    public function testUpdate()
    {
        $qaTopicData = factory(QaTopic::class)->create();

        /** @var  \App\Repositories\QaTopicRepositoryInterface $repository */
        $repository = \App::make(\App\Repositories\QaTopicRepositoryInterface::class);
        $this->assertNotNull($repository);

        $qaTopicCheck = $repository->update($qaTopicData, $qaTopicData->toFillableArray());
        $this->assertNotNull($qaTopicCheck);
    }

    public function testDelete()
    {
        $qaTopicData = factory(QaTopic::class)->create();

        /** @var  \App\Repositories\QaTopicRepositoryInterface $repository */
        $repository = \App::make(\App\Repositories\QaTopicRepositoryInterface::class);
        $this->assertNotNull($repository);

        $repository->delete($qaTopicData);

        $qaTopicCheck = $repository->find($qaTopicData->id);
        $this->assertNull($qaTopicCheck);
    }

}
