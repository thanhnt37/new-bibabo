<?php  namespace Tests\Controllers\Admin;

use Tests\TestCase;

class EventControllerTest extends TestCase
{

    protected $useDatabase = true;

    public function testGetInstance()
    {
        /** @var  \App\Http\Controllers\Admin\EventController $controller */
        $controller = \App::make(\App\Http\Controllers\Admin\EventController::class);
        $this->assertNotNull($controller);
    }

    public function setUp()
    {
        parent::setUp();
        $authUser = \App\Models\AdminUser::first();
        $this->be($authUser, 'admins');
    }

    public function testGetList()
    {
        $response = $this->action('GET', 'Admin\EventController@index');
        $this->assertResponseOk();
    }

    public function testCreateModel()
    {
        $this->action('GET', 'Admin\EventController@create');
        $this->assertResponseOk();
    }

    public function testStoreModel()
    {
        $event = factory(\App\Models\Event::class)->make();
        $this->action('POST', 'Admin\EventController@store', [
                '_token' => csrf_token(),
            ] + $event->toArray());
        $this->assertResponseStatus(302);
    }

    public function testEditModel()
    {
        $event = factory(\App\Models\Event::class)->create();
        $this->action('GET', 'Admin\EventController@show', [$event->id]);
        $this->assertResponseOk();
    }

    public function testUpdateModel()
    {
        $faker = \Faker\Factory::create();

        $event = factory(\App\Models\Event::class)->create();

        $name = $faker->name;
        $id = $event->id;

        $event->name = $name;

        $this->action('PUT', 'Admin\EventController@update', [$id], [
                '_token' => csrf_token(),
            ] + $event->toArray());
        $this->assertResponseStatus(302);

        $newEvent = \App\Models\Event::find($id);
        $this->assertEquals($name, $newEvent->name);
    }

    public function testDeleteModel()
    {
        $event = factory(\App\Models\Event::class)->create();

        $id = $event->id;

        $this->action('DELETE', 'Admin\EventController@destroy', [$id], [
                '_token' => csrf_token(),
            ]);
        $this->assertResponseStatus(302);

        $checkEvent = \App\Models\Event::find($id);
        $this->assertNull($checkEvent);
    }

}
