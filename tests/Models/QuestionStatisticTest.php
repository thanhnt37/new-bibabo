<?php namespace Tests\Models;

use App\Models\QuestionStatistic;
use Tests\TestCase;

class QuestionStatisticTest extends TestCase
{

    protected $useDatabase = true;

    public function testGetInstance()
    {
        /** @var  \App\Models\QuestionStatistic $questionStatistic */
        $questionStatistic = new QuestionStatistic();
        $this->assertNotNull($questionStatistic);
    }

    public function testStoreNew()
    {
        /** @var  \App\Models\QuestionStatistic $questionStatistic */
        $questionStatisticModel = new QuestionStatistic();

        $questionStatisticData = factory(QuestionStatistic::class)->make();
        foreach( $questionStatisticData->toFillableArray() as $key => $value ) {
            $questionStatisticModel->$key = $value;
        }
        $questionStatisticModel->save();

        $this->assertNotNull(QuestionStatistic::find($questionStatisticModel->id));
    }

}
