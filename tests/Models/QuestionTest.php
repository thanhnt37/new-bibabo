<?php namespace Tests\Models;

use App\Models\Question;
use Tests\TestCase;

class QuestionTest extends TestCase
{

    protected $useDatabase = true;

    public function testGetInstance()
    {
        /** @var  \App\Models\Question $question */
        $question = new Question();
        $this->assertNotNull($question);
    }

    public function testStoreNew()
    {
        /** @var  \App\Models\Question $question */
        $questionModel = new Question();

        $questionData = factory(Question::class)->make();
        foreach( $questionData->toFillableArray() as $key => $value ) {
            $questionModel->$key = $value;
        }
        $questionModel->save();

        $this->assertNotNull(Question::find($questionModel->id));
    }

}
