<?php namespace Tests\Models;

use App\Models\QaTopicLike;
use Tests\TestCase;

class QaTopicLikeTest extends TestCase
{

    protected $useDatabase = true;

    public function testGetInstance()
    {
        /** @var  \App\Models\QaTopicLike $qaTopicLike */
        $qaTopicLike = new QaTopicLike();
        $this->assertNotNull($qaTopicLike);
    }

    public function testStoreNew()
    {
        /** @var  \App\Models\QaTopicLike $qaTopicLike */
        $qaTopicLikeModel = new QaTopicLike();

        $qaTopicLikeData = factory(QaTopicLike::class)->make();
        foreach( $qaTopicLikeData->toFillableArray() as $key => $value ) {
            $qaTopicLikeModel->$key = $value;
        }
        $qaTopicLikeModel->save();

        $this->assertNotNull(QaTopicLike::find($qaTopicLikeModel->id));
    }

}
