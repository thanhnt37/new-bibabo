<?php namespace Tests\Models;

use App\Models\QaTopic;
use Tests\TestCase;

class QaTopicTest extends TestCase
{

    protected $useDatabase = true;

    public function testGetInstance()
    {
        /** @var  \App\Models\QaTopic $qaTopic */
        $qaTopic = new QaTopic();
        $this->assertNotNull($qaTopic);
    }

    public function testStoreNew()
    {
        /** @var  \App\Models\QaTopic $qaTopic */
        $qaTopicModel = new QaTopic();

        $qaTopicData = factory(QaTopic::class)->make();
        foreach( $qaTopicData->toFillableArray() as $key => $value ) {
            $qaTopicModel->$key = $value;
        }
        $qaTopicModel->save();

        $this->assertNotNull(QaTopic::find($qaTopicModel->id));
    }

}
