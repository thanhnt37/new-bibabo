<?php namespace App\Repositories\Eloquent;

use \App\Repositories\QaTopicRepositoryInterface;
use \App\Models\QaTopic;

class QaTopicRepository extends SingleKeyModelRepository implements QaTopicRepositoryInterface
{

    public function getBlankModel()
    {
        return new QaTopic();
    }

    public function rules()
    {
        return [
        ];
    }

    public function messages()
    {
        return [
        ];
    }

    /**
     * Return all main groups
     * */
    public function getAllGroups()
    {
        $level0 = $this->allByQaTopicParentId(QaTopic::QA_ROOT_TOPIC_CLUB, 'qa_topic_id', 'asc')->pluck('qa_topic_id')->toArray();

        $query = $this->getBlankModel();
        $query = $query->whereIn('qa_topic_parent_id', $level0)
            ->where('qa_topic_del_flag', 0)
            ->where('qa_topic_status', 1)
            ->orderBy('qa_topic_number_question', 'desc');
        
        return $query->get();
    }
}
