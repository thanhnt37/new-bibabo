<?php namespace App\Repositories\Eloquent;

use \App\Repositories\EventRepositoryInterface;
use \App\Models\Event;

class EventRepository extends SingleKeyModelRepository implements EventRepositoryInterface
{

    public function getBlankModel()
    {
        return new Event();
    }

    public function rules()
    {
        return [
        ];
    }

    public function messages()
    {
        return [
        ];
    }

}
