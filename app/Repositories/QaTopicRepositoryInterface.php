<?php namespace App\Repositories;

interface QaTopicRepositoryInterface extends SingleKeyModelRepositoryInterface
{
    /**
     * Return all main groups
     * */
    public function getAllGroups();
}