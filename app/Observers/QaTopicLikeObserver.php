<?php namespace App\Observers;

use Illuminate\Support\Facades\Redis;

class QaTopicLikeObserver extends BaseObserver
{
    protected $cachePrefix = 'QaTopicLikeModel';

    public function created($model)
    {
        if( \CacheHelper::cacheRedisEnabled() ) {
            
        }
    }

    public function updated($model)
    {
        if( \CacheHelper::cacheRedisEnabled() ) {
            
        }
    }

    public function deleted($model)
    {
        if( \CacheHelper::cacheRedisEnabled() ) {
            
        }
    }
}