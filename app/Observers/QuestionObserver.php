<?php namespace App\Observers;

use Illuminate\Support\Facades\Redis;

class QuestionObserver extends BaseObserver
{
    protected $cachePrefix = 'QuestionModel';

    public function created($model)
    {
        if( \CacheHelper::cacheRedisEnabled() ) {
            $cacheKey = \CacheHelper::keyForModel($this->cachePrefix);
            Redis::hsetnx($cacheKey, $model->question_id, $model);

            $this->increaseNumberParticipants($model);
        }
    }

    public function updated($model)
    {
        if( \CacheHelper::cacheRedisEnabled() ) {
            $cacheKey = \CacheHelper::keyForModel($this->cachePrefix);
            Redis::hset($cacheKey, $model->question_id, $model);
        }
    }

    public function deleted($model)
    {
        if( \CacheHelper::cacheRedisEnabled() ) {
            $this->decreaseNumberParticipants($model);

            $cacheKey = \CacheHelper::keyForModel($this->cachePrefix);
            Redis::hdel($cacheKey, $model->question_id);
        }
    }

    private function increaseNumberParticipants($model)
    {
        $cacheKey = \CacheHelper::keyForMethod('QaTopicPresenter', 'getNumberParticipants', [$model->qa_club_id]);
        $numberParticipants = Redis::get($cacheKey);

        $numberParticipants += 1;
        Redis::set($cacheKey, $numberParticipants);
        // Redis::expire($cacheKey, 300);  // 5 minutes

        return $numberParticipants;
    }

    private function decreaseNumberParticipants($model)
    {
        $cacheKey = \CacheHelper::keyForMethod('QaTopicPresenter', 'getNumberParticipants', [$model->qa_club_id]);
        $numberParticipants = Redis::get($cacheKey);

        $numberParticipants -= 1;
        Redis::set($cacheKey, $numberParticipants);
        // Redis::expire($cacheKey, 300);  // 5 minutes

        return $numberParticipants;
    }
}