<?php namespace App\Observers;

use Illuminate\Support\Facades\Redis;

class SiteConfigurationObserver extends BaseObserver
{
    protected $cachePrefix = 'SiteConfigurationModel';

    public function created($user)
    {

    }

    public function updated($user)
    {

    }

    public function deleted($user)
    {

    }
}