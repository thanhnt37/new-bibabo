<?php
namespace App\Models;

class QuestionStatistic extends Base
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'question_statistic';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'question_id',
        'user_id',
        'question_created_at',
        'question_statistic_number_viewed',
        'question_statistic_number_viewed_ano',
        'question_statistic_number_like',
        'question_statistic_number_answer',
        'question_statistic_number_like_answer',
        'question_statistic_number_comment',
        'question_statistic_number_like_comment',
        'question_statistic_number_share_fb',
        'question_statistic_latitude',
        'question_statistic_longitude',
        'question_statistic_answer_hot',
        'first_answer_at',
        'question_statistic_created_platform',
        'language_id',
        'question_statistic_del_flag',
        'feed_extra_answer_id',
        'on_create_send_notify_count',
        'question_statistic_saved_count',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    protected $dates  = [];

    protected $presenter = \App\Presenters\QuestionStatisticPresenter::class;

    public static function boot()
    {
        parent::boot();
        parent::observe(new \App\Observers\QuestionStatisticObserver);
    }

    // Relations
    public function question()
    {
        return $this->belongsTo(\App\Models\Question::class, 'question_id', 'question_id');
    }

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class, 'user_id', 'user_id');
    }
    

    // Utility Functions

    /*
     * API Presentation
     */
    public function toAPIArray()
    {
        return [
            'question_id'                            => $this->question_id,
            'user_id'                                => $this->user_id,
            'question_created_at'                    => $this->question_created_at,
            'question_statistic_number_viewed'       => $this->question_statistic_number_viewed,
            'question_statistic_number_viewed_ano'   => $this->question_statistic_number_viewed_ano,
            'question_statistic_number_like'         => $this->question_statistic_number_like,
            'question_statistic_number_answer'       => $this->question_statistic_number_answer,
            'question_statistic_number_like_answer'  => $this->question_statistic_number_like_answer,
            'question_statistic_number_comment'      => $this->question_statistic_number_comment,
            'question_statistic_number_like_comment' => $this->question_statistic_number_like_comment,
            'question_statistic_number_share_fb'     => $this->question_statistic_number_share_fb,
            'question_statistic_latitude'            => $this->question_statistic_latitude,
            'question_statistic_longitude'           => $this->question_statistic_longitude,
            'question_statistic_answer_hot'          => $this->question_statistic_answer_hot,
            'first_answer_at'                        => $this->first_answer_at,
            'question_statistic_created_platform'    => $this->question_statistic_created_platform,
            'language_id'                            => $this->language_id,
            'question_statistic_del_flag'            => $this->question_statistic_del_flag,
            'feed_extra_answer_id'                   => $this->feed_extra_answer_id,
            'on_create_send_notify_count'            => $this->on_create_send_notify_count,
            'question_statistic_saved_count'         => $this->question_statistic_saved_count,
        ];
    }

}
