<?php
namespace App\Models;

class QaTopic extends Base
{
    const QA_TOPIC_STATUS_ACTIVE = 1;
    const QA_TOPIC_STATUS_HIDE = 0;

    //Level -1
    const QA_ROOT_TOPIC_KNOWLEDGE   = 180;
    const QA_ROOT_TOPIC_CLUB        = 181;
    const QA_ROOT_FEATURES          = 182;

    const QA_TOPIC_TYPE_NORMAL          = 0;
    const QA_TOPIC_TYPE_CLUB_POLICY     = 1;//qa_topic_type
    const QA_TOPIC_TYPE_FUNNEL          = 2;
    const QA_TOPIC_TYPE_CLUB_OTHER      = 3;
    const QA_TOPIC_TYPE_HEALTH_CATEGORY = 4;
    const QA_TOPIC_TYPE_EVENT           = 5;//sự kiện
    const QA_TOPIC_TYPE_GALLERY         = 6;//thư viện
    const QA_TOPIC_TYPE_COMPETITION     = 7;//Cuộc thi

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'qa_topic';

    protected $primaryKey = 'qa_topic_id';

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'qa_topic_is_parent',
        'qa_topic_parent_id',
        'qa_topic_deep_level',
        'qa_topic_list_all_child_id',
        'qa_topic_sort',
        'qa_topic_name',
        'qa_topic_shortname',
        'qa_topic_fullname',
        'qa_topic_desc',
        'qa_topic_parent',
        'qa_topic_fixed',
        'qa_topic_month',
        'qa_topic_number_question',
        'qa_topic_number_answer',
        'qa_topic_number_like',
        'qa_topic_number_assistant',
        'language_id',
        'qa_topic_status',
        'qa_topic_created_at',
        'admin_id',
        'user_id',
        'qa_topic_image',
        'qa_topic_del_flag',
        'qa_topic_type',
        'qa_topic_category',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    protected $dates  = [];

    protected $presenter = \App\Presenters\QaTopicPresenter::class;

    public static function boot()
    {
        parent::boot();
        parent::observe(new \App\Observers\QaTopicObserver);
    }

    // Relations
    public function qaTopicParent()
    {
//        return $this->belongsTo(\App\Models\QaTopicParent::class, 'qa_topic_parent_id', 'id');
    }

    public function language()
    {
//        return $this->belongsTo(\App\Models\Language::class, 'language_id', 'id');
    }

    public function author()
    {
        return $this->belongsTo(\App\Models\User::class, 'user_id', 'user_id');
    }

    public function participants()
    {
        return $this->hasMany(\App\Models\Question::class, 'qa_club_id', 'qa_topic_id');
    }

    public function subscribed()
    {
        return $this->hasMany(\App\Models\QaTopicLike::class, 'qa_topic_id', 'qa_topic_id')->where('qa_topic_like_is_unlike', 0);
    }

    

    // Utility Functions

    /*
     * API Presentation
     */
    public function toAPIArray()
    {
        return [
            'qa_topic_is_parent'         => $this->qa_topic_is_parent,
            'qa_topic_parent_id'         => $this->qa_topic_parent_id,
            'qa_topic_deep_level'        => $this->qa_topic_deep_level,
            'qa_topic_list_all_child_id' => $this->qa_topic_list_all_child_id,
            'qa_topic_sort'              => $this->qa_topic_sort,
            'qa_topic_name'              => $this->qa_topic_name,
            'qa_topic_shortname'         => $this->qa_topic_shortname,
            'qa_topic_fullname'          => $this->qa_topic_fullname,
            'qa_topic_desc'              => $this->qa_topic_desc,
            'qa_topic_parent'            => $this->qa_topic_parent,
            'qa_topic_fixed'             => $this->qa_topic_fixed,
            'qa_topic_month'             => $this->qa_topic_month,
            'qa_topic_number_question'   => $this->qa_topic_number_question,
            'qa_topic_number_answer'     => $this->qa_topic_number_answer,
            'qa_topic_number_like'       => $this->qa_topic_number_like,
            'qa_topic_number_assistant'  => $this->qa_topic_number_assistant,
            'language_id'                => $this->language_id,
            'qa_topic_status'            => $this->qa_topic_status,
            'qa_topic_created_at'        => $this->qa_topic_created_at,
            'admin_id'                   => $this->admin_id,
            'user_id'                    => $this->user_id,
            'qa_topic_image'             => $this->qa_topic_image,
            'qa_topic_del_flag'          => $this->qa_topic_del_flag,
            'qa_topic_type'              => $this->qa_topic_type,
            'qa_topic_category'          => $this->qa_topic_category,
        ];
    }

}
