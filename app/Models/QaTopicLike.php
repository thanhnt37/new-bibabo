<?php
namespace App\Models;

class QaTopicLike extends Base
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'qa_topic_like';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'qa_topic_id',
        'qa_topic_like_is_unlike',
        'qa_topic_like_created_at',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    protected $dates  = [];

    protected $presenter = \App\Presenters\QaTopicLikePresenter::class;

    public static function boot()
    {
        parent::boot();
        parent::observe(new \App\Observers\QaTopicLikeObserver);
    }

    // Relations
    public function user()
    {
        return $this->belongsTo(\App\Models\User::class, 'user_id', 'id');
    }

    public function qaTopic()
    {
        return $this->belongsTo(\App\Models\QaTopic::class, 'qa_topic_id', 'id');
    }

    

    // Utility Functions

    /*
     * API Presentation
     */
    public function toAPIArray()
    {
        return [
            'id'                       => $this->id,
            'user_id'                  => $this->user_id,
            'qa_topic_id'              => $this->qa_topic_id,
            'qa_topic_like_is_unlike'  => $this->qa_topic_like_is_unlike,
            'qa_topic_like_created_at' => $this->qa_topic_like_created_at,
        ];
    }

}
