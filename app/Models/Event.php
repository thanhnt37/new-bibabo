<?php
namespace App\Models;

class Event extends Base
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'qa_topic_event';

    protected $primaryKey = 'topic_event_id';

    public $timestamps = false;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'qa_topic_id',
        'event_name',
        'event_template_name',
        'event_master_id',
        'event_status',
        'event_type',
        'event_created_at',
        'event_start_at',
        'event_end_at',
        'event_priority_qids',
        'event_product_ids',
        'event_banner_url',
        'event_rank',
        'event_like_point',
        'event_share_point',
        'event_comment_point',
        'event_club_ids',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    protected $dates  = ['event_created_at', 'event_start_at', 'event_end_at'];

    protected $presenter = \App\Presenters\EventPresenter::class;

    public static function boot()
    {
        parent::boot();
        parent::observe(new \App\Observers\EventObserver);
    }

    // Relations
    public function topic()
    {
        return $this->belongsTo(\App\Models\QaTopic::class, 'qa_topic_id', 'qa_topic_id');
    }

    

    // Utility Functions

    /*
     * API Presentation
     */
    public function toAPIArray()
    {
        return [
            'qa_topic_id'         => $this->qa_topic_id,
            'event_name'          => $this->event_name,
            'event_template_name' => $this->event_template_name,
            'event_master_id'     => $this->event_master_id,
            'event_status'        => $this->event_status,
            'event_type'          => $this->event_type,
            'event_created_at'    => $this->event_created_at,
            'event_start_at'      => $this->event_start_at,
            'event_end_at'        => $this->event_end_at,
            'event_priority_qids' => $this->event_priority_qids,
            'event_product_ids'   => $this->event_product_ids,
            'event_banner_url'    => $this->event_banner_url,
            'event_rank'          => $this->event_rank,
            'event_like_point'    => $this->event_like_point,
            'event_share_point'   => $this->event_share_point,
            'event_comment_point' => $this->event_comment_point,
            'event_club_ids'      => $this->event_club_ids,
        ];
    }

}
