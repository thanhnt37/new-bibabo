<?php namespace App\Models;



class Question extends Base
{

    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'question';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'question_question',
        'question_desc',
        'question_image',
        'content_type',
        'question_approved_user_level',
        'user_id',
        'month_no',
        'week_no',
        'question_created_at',
        'question_del_flag',
        'question_type',
        'question_status',
        'language_id',
        'question_last_action',
        'qa_club_id',
        'assigned_mod_id',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    protected $dates  = [];

    protected $presenter = \App\Presenters\QuestionPresenter::class;

    public static function boot()
    {
        parent::boot();
        parent::observe(new \App\Observers\QuestionObserver);
    }

    // Relations
    public function author()
    {
        return $this->belongsTo(\App\Models\User::class, 'user_id', 'user_id');
    }

    public function language()
    {
//        return $this->belongsTo(\App\Models\Language::class, 'language_id', 'id');
    }

    public function statistics()
    {
        return $this->hasOne(\App\Models\QuestionStatistic::class, 'question_id', 'question_id');
    }

    

    // Utility Functions

    /*
     * API Presentation
     */
    public function toAPIArray()
    {
        return [
            'id'                           => $this->id,
            'question_question'            => $this->question_question,
            'question_desc'                => $this->question_desc,
            'question_image'               => $this->question_image,
            'content_type'                 => $this->content_type,
            'question_approved_user_level' => $this->question_approved_user_level,
            'user_id'                      => $this->user_id,
            'month_no'                     => $this->month_no,
            'week_no'                      => $this->week_no,
            'question_created_at'          => $this->question_created_at,
            'question_del_flag'            => $this->question_del_flag,
            'question_type'                => $this->question_type,
            'question_status'              => $this->question_status,
            'language_id'                  => $this->language_id,
            'question_last_action'         => $this->question_last_action,
            'qa_club_id'                   => $this->qa_club_id,
            'assigned_mod_id'              => $this->assigned_mod_id,
        ];
    }

}
