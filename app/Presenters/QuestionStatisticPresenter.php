<?php

namespace App\Presenters;

use Illuminate\Support\Facades\Redis;
use App\Models\Question;
use App\Models\User;
use App\Models\Language;
use App\Models\FeedExtraAnswer;

class QuestionStatisticPresenter extends BasePresenter
{
    protected $multilingualFields = [];

    protected $imageFields = [];

}
