<?php

namespace App\Presenters;

use Illuminate\Support\Facades\Redis;
use App\Models\Question;
use App\Models\User;
use App\Models\Language;
use App\Models\QaClub;
use App\Models\AssignedMod;

class QuestionPresenter extends BasePresenter
{
    protected $multilingualFields = [];

    protected $imageFields = [];
    
    /**
     * @return \App\Models\User
     * */
    public function getAuthor()
    {
        if( \CacheHelper::cacheRedisEnabled() ) {
            $cacheKey = \CacheHelper::keyForModel('UserModel');
            $cached = Redis::hget($cacheKey, $this->entity->user_id);

            if( $cached ) {
                $user = new User(json_decode($cached, true));
                $user['user_id'] = json_decode($cached, true)['user_id'];
                return $user;
            } else {
                $user = $this->entity->author;
                Redis::hsetnx($cacheKey, $this->entity->user_id, $user);
                return $user;
            }
        }

        $user = $this->entity->author;
        return $user;
    }
}
