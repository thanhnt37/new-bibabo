<?php

namespace App\Presenters;

use Illuminate\Support\Facades\Redis;
use App\Models\QaTopic;

class EventPresenter extends BasePresenter
{
    protected $multilingualFields = [];

    protected $imageFields = [];

    /**
    * @return \App\Models\QaTopic
    * */
    public function getTopic()
    {
        if( \CacheHelper::cacheRedisEnabled() ) {
            $cacheKey = \CacheHelper::keyForModel('QaTopicModel');
            $cached = Redis::hget($cacheKey, $this->entity->qa_topic_id);

            if( $cached ) {
                $qaTopic = new QaTopic(json_decode($cached, true));
                $qaTopic['qa_topic_id'] = json_decode($cached, true)['qa_topic_id'];
                return $qaTopic;
            } else {
                $qaTopic = $this->entity->topic;
                Redis::hsetnx($cacheKey, $this->entity->qa_topic_id, $qaTopic);
                return $qaTopic;
            }
        }

        $qaTopic = $this->entity->topic;
        return $qaTopic;
    }

    /**
    * @return string
    * */
    public function getEventType()
    {
        $event = $this->entity;

        if($event->event_type == 1){
            return 'Cuộc thi ảnh';
        } elseif($event->event_type == 1) {
            return 'Cuộc thi viết';
        } else {
            return 'Undefined';
        }
    }
}
