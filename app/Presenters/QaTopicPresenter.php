<?php

namespace App\Presenters;

use Illuminate\Support\Facades\Redis;
use App\Models\QaTopic;
use App\Models\QaTopicParent;
use App\Models\QaTopicListAllChild;
use App\Models\Language;
use App\Models\Admin;
use App\Models\User;

class QaTopicPresenter extends BasePresenter
{
    protected $multilingualFields = [];

    protected $imageFields = [];

    /**
    * @return \App\Models\User
    * */
    public function getAuthor()
    {
        if( \CacheHelper::cacheRedisEnabled() ) {
            $cacheKey = \CacheHelper::keyForModel('UserModel');
            $cached = Redis::hget($cacheKey, $this->entity->user_id);

            if( $cached ) {
                $user = new User(json_decode($cached, true));
                $user['user_id'] = json_decode($cached, true)['user_id'];
                return $user;
            } else {
                $user = $this->entity->author;
                Redis::hsetnx($cacheKey, $this->entity->user_id, $user);
                return $user;
            }
        }

        $user = $this->entity->author;
        return $user;
    }

    /**
    * @return int
    * */
    // Cần xử lý cache khi có thao tác crud với bảng qa_topic_like
    public function getNumberSubscribed()
    {
        if( \CacheHelper::cacheRedisEnabled() ) {
            $cacheKey = \CacheHelper::keyForMethod('QaTopicPresenter', 'getNumberSubscribed', [$this->entity->user_id]);
            $numberSubscribed = Redis::get($cacheKey);

            if( isset($numberSubscribed) ) {
                return $numberSubscribed;
            } else {
                $numberSubscribed = count($this->entity->subscribed);
                Redis::set($cacheKey, $numberSubscribed);
                // Redis::expire($cacheKey, 300);  // 5 minutes
                return $numberSubscribed;
            }
        }

        $numberSubscribed = count($this->entity->subscribed);
        return $numberSubscribed;
    }

    /**
    * @return int
    * */
    // Cần xử lý cache khi có thao tác crud với bảng question
    public function getNumberParticipants()
    {
        if( \CacheHelper::cacheRedisEnabled() ) {
            $cacheKey = \CacheHelper::keyForMethod('QaTopicPresenter', 'getNumberParticipants', [$this->entity->user_id]);
            $numberParticipants = Redis::get($cacheKey);

            if( isset($numberParticipants) ) {
                return $numberParticipants;
            } else {
                $numberParticipants = count($this->entity->participants);
                Redis::set($cacheKey, $numberParticipants);
                // Redis::expire($cacheKey, 300);  // 5 minutes
                return $numberParticipants;
            }
        }

        $numberParticipants = count($this->entity->subscribed);
        return $numberParticipants;
    }
}
