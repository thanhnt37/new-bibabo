<?php

namespace App\Presenters;

use Illuminate\Support\Facades\Redis;
use App\Models\QaTopicLike;
use App\Models\User;
use App\Models\QaTopic;

class QaTopicLikePresenter extends BasePresenter
{
    protected $multilingualFields = [];

    protected $imageFields = [];
}
