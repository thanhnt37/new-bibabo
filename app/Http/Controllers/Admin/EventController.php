<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\AdminUser;
use App\Models\Event;
use App\Models\QaTopic;
use App\Repositories\EventRepositoryInterface;
use App\Http\Requests\Admin\EventRequest;
use App\Http\Requests\PaginationRequest;
use App\Repositories\QaTopicRepositoryInterface;
use Illuminate\Support\Facades\Auth;
use App\Services\AdminUserServiceInterface;

class EventController extends Controller
{
    /** @var \App\Repositories\EventRepositoryInterface */
    protected $eventRepository;

    /** @var \App\Repositories\QaTopicRepositoryInterface */
    protected $qaTopicRepository;

    /** @var \App\Services\AdminUserServiceInterface */
    protected $adminUserService;


    public function __construct(
        EventRepositoryInterface    $eventRepository,
        QaTopicRepositoryInterface  $qaTopicRepository,
        AdminUserServiceInterface   $adminUserService
    )
    {
        $this->eventRepository      = $eventRepository;
        $this->qaTopicRepository    = $qaTopicRepository;
        $this->adminUserService     = $adminUserService;
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \App\Http\Requests\PaginationRequest $request
     * @return \Response
     */
    public function index(PaginationRequest $request)
    {
        $paginate['offset']     = $request->offset();
        $paginate['limit']      = $request->limit();
        $paginate['order']      = $request->order('topic_event_id');
        $paginate['direction']  = $request->direction();
        $paginate['baseUrl']    = action( 'Admin\EventController@index' );

        $count = $this->eventRepository->count();
        $events = $this->eventRepository->get( $paginate['order'], $paginate['direction'], $paginate['offset'], $paginate['limit'] );

        return view(
            'pages.admin.' . config('view.admin') . '.events.index',
            [
                'events'   => $events,
                'count'    => $count,
                'paginate' => $paginate,
            ]
        );
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Response
     */
    public function create()
    {
        return view(
            'pages.admin.' . config('view.admin') . '.events.edit',
            [
                'isNew'  => true,
                'event'  => $this->eventRepository->getBlankModel(),
                'groups' => $this->qaTopicRepository->getAllGroups()
            ]
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  $request
     * @return \Response
     */
    public function store(EventRequest $request)
    {
        $input = $request->only(
            [
                'event_name',
                'event_start_at',
                'event_end_at',
                'event_like_point',
                'event_share_point',
                'event_comment_point',
                'event_status',
                'event_rank',
                'event_type',
            ]
        );

        $admin = $this->adminUserService->getUser();

        $descriptions = $request->get('event_description', '');
        $topic = $this->qaTopicRepository->create(
            [
                'qa_topic_deep_level' => 1,
                'qa_topic_name'       => $input['event_name'],
                'qa_topic_desc'       => $descriptions,
                'user_id'             => $admin->user_id,
                'qa_topic_type'       => QaTopic::QA_TOPIC_TYPE_COMPETITION,
                'qa_topic_created_at' => date('Y-m-d H:i:s'),
            ]
        );
        $input['qa_topic_id']      = $topic->qa_topic_id;
        $input['event_created_at'] = date('Y-m-d H:i:s');
        $input['event_start_at']   = strtotime($input['event_start_at']);
        $input['event_end_at']     = strtotime($input['event_end_at']) ? strtotime($input['event_end_at']) : 0;
        $input['event_master_id']  = 0;

        $clubs = $request->get('event_club_ids', []);
        $input['event_club_ids'] = implode(',', $clubs);

        $event = $this->eventRepository->create($input);

        if (empty( $event )) {
            return redirect()->back()->withErrors(trans('admin.errors.general.save_failed'));
        }

        return redirect()->action('Admin\EventController@index')
            ->with('message-success', trans('admin.messages.general.create_success'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Response
     */
    public function show($id)
    {
        $event = $this->eventRepository->find($id);
        if (empty( $event )) {
            abort(404);
        }

        $groups = $this->qaTopicRepository->getAllGroups();

        return view(
            'pages.admin.' . config('view.admin') . '.events.edit',
            [
                'isNew'  => false,
                'event'  => $event,
                'groups' => $groups
            ]
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param      $request
     * @return \Response
     */
    public function update($id, EventRequest $request)
    {

        /** @var \App\Models\Event $event */
        $event = $this->eventRepository->find($id);
        if (empty( $event )) {
            abort(404);
        }

        $input = $request->only(
            [
                'event_name',
                'event_start_at',
                'event_end_at',
                'event_like_point',
                'event_share_point',
                'event_comment_point',
                'event_status',
                'event_rank',
                'event_type',
            ]
        );
        $input['event_start_at']   = strtotime($input['event_start_at']);
        $input['event_end_at']     = strtotime($input['event_end_at']) ? strtotime($input['event_end_at']) : 0;

        $clubs = $request->get('event_club_ids', []);
        $input['event_club_ids'] = implode(',', $clubs);

        $descriptions = $request->get('event_description', '');
        $topic = $this->qaTopicRepository->update(
            $event->topic,
            [
                'qa_topic_name'       => $input['event_name'],
                'qa_topic_desc'       => $descriptions,
            ]
        );

        $this->eventRepository->update($event, $input);

        return redirect()->action('Admin\EventController@show', [$id])
                    ->with('message-success', trans('admin.messages.general.update_success'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Response
     */
    public function destroy($id)
    {
        /** @var \App\Models\Event $event */
        $event = $this->eventRepository->find($id);
        if (empty( $event )) {
            abort(404);
        }
        $this->eventRepository->delete($event);

        return redirect()->action('Admin\EventController@index')
                    ->with('message-success', trans('admin.messages.general.delete_success'));
    }

    public function participants($id, PaginationRequest $request)
    {
        /** @var \App\Models\Event $event */
        $event = $this->eventRepository->find($id);
        if (empty( $event )) {
            abort(404);
        }

        $paginate['offset']     = $request->offset();
        $paginate['limit']      = $request->limit();
        $paginate['order']      = $request->order('question_id');
        $paginate['direction']  = $request->direction('desc');
        $paginate['baseUrl']    = action('Admin\EventController@participants', $id);

        $examples = $event->topic()->first()->participants();
        $count    = $examples->count();
        $examples = $examples->orderBy($paginate['order'], $paginate['direction'])->offset($paginate['offset'])->limit($paginate['limit'])->get();

        return view(
            'pages.admin.' . config('view.admin') . '.events.participants',
            [
                'event'    => $event,
                'examples' => $examples,
                'count'    => $count,
                'paginate' => $paginate,
            ]
        );
    }
}
