<?php namespace App\Http\Requests\Admin;

use App\Http\Requests\BaseRequest;
use App\Repositories\EventRepositoryInterface;

class EventRequest extends BaseRequest
{

    /** @var \App\Repositories\EventRepositoryInterface */
    protected $eventRepository;

    public function __construct(EventRepositoryInterface $eventRepository)
    {
        $this->eventRepository = $eventRepository;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'event_name'          => 'required|string',
            'event_like_point'    => 'required|numeric|min:0',
            'event_share_point'   => 'required|numeric|min:0',
            'event_comment_point' => 'required|numeric|min:0',
            'event_status'        => 'required|numeric|min:0|max:1',
            'event_rank'          => 'required|numeric|min:0|max:1',
            'event_type'          => 'required|numeric|min:0|max:1',
            'event_description'   => 'required|string',
            'event_club_ids'      => 'required|array',
            'event_start_at'      => 'date_format:Y-m-d H:i:s|required',
            'event_end_at'        => 'date_format:Y-m-d H:i:s|nullable',
        ];

        return $rules;
    }

    public function messages()
    {
        return $this->eventRepository->messages();
    }

}
